
#include <stdio.h>
#include <math.h>
float discriminate(float, float, float);
float root_1(float, float, float);
float root_2(float, float, float);

int main(){
    float a, b, c;
    printf("Enter the coeficient of a:\n");
    scanf("%f", &a);
    if(a == 0){
        printf("ERROR:the cofficient a can't be zero\nEnter another coeficient of a:\n");
        scanf("%f", &a);
    }
    printf("Enter the coeficient of b:\n");
    scanf("%f", &b);
    printf("Enter the coeficient of c:\n");
    scanf("%f", &c);
    
    
    
    
    float _discriminate = discriminate(a,b,c);
    if (_discriminate < 0) {
        printf("The quadratic has imaginery roots");
    } else if (_discriminate > 0){
        printf("The roots of the quadratic are distinct %.2f, %.2f", root_1(a,b,c), root_2(a,b,c));
    } else {
        printf("The quadratic has equal roots %.2f", root_1(a,b,c));
    }
    return 0;
}


float discriminate(float a, float b, float c){
        return pow(b,2) - (4 * a * c);
}

float root_1 (float a, float b, float c) {
    return ((-b)- sqrt(discriminate(a,b,c))) / (2*a);
}

float root_2 (float a, float b, float c) {
    return ((-b) + sqrt(discriminate(a,b,c))) / (2*a);
}
